const nav = document.querySelector('nav');
const navTop = nav.offsetTop;
const body = document.querySelector('body');

function stickyNav() {
    
    // SI window.scrollY est supérieur ou égal à nav.offsetTop, alors on rend la nav fixe
    if(window.scrollY >= navTop){
        nav.classList.add('fixed');
        body.style.paddingTop = nav.clientHeight + 'px';        
    }
    // SINON (window.scrollY est inférieur à nav.offsetTop), on rend la nav statique
    else{
        nav.classList.remove('fixed');
        body.style.paddingTop = 0;
    }
    
}

window.addEventListener('scroll', stickyNav);